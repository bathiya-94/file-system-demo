package com.rtjvm.scala.oop.commands

import com.rtjvm.scala.oop.files.{DirEntry, Directory}
import com.rtjvm.scala.oop.filesystem.State

abstract class CreateEntry(name: String) extends Command {
  override def apply(state: State): State = {
    val wd = state.wd
    if (wd.hasEntry(name)) {
      state.setMessage("Entry " + name + " already exists!")
    } else if (name.contains(Directory.SEPARATOR)) {
      state.setMessage(name + "must not contain separators")
    } else if (checkIllegal(name)) {
      state.setMessage(name + ": illegal entry name")
    } else {
      doCreateEntry(state, name)
    }
  }

  def checkIllegal(name: String): Boolean = {
    name.contains(".")
  }

  def doCreateEntry(state: State, name: String): State = {
    def updateStructure(currentDirectory: Directory, path: List[String], newEntry: DirEntry):Directory = {
      /*
        someDit
        /a
        /b
        / (new) /d
        => new someDir
        /a
        /b
        /d
       */
      if (path.isEmpty) currentDirectory.addEntry(newEntry)
      else {
        /*
        /a/b
         /c
         /d
           current directory = /a
           path =["b"]
         */
        val oldEntry = currentDirectory.findEntry(path.head).asDirectory
        currentDirectory.replaceEntry(oldEntry.name, updateStructure(oldEntry,path.tail,newEntry))

        /*
            /a/b
            (contents)
            (enw entry) /e

          new Root =  updateStructure(root, ["a",, "b"], /e)
            => path.IsEmpty
            => oldEntry =/b
            /a.replaceEntry("b, updateStructure(/b [], /a)
             => path.isEmpty ? = > /b.add(/a
         */
      }
    }

    val wd = state.wd
    // 1. all the directories in the full path
    val allDirsInPath = wd.getAllFoldersInPath
    // 2. create the directory entry in the wd
    val newEntry = createSpecificEntry(state)
    // 3. Update the whole directory structure starting from the root
    val newRoot = updateStructure(state.root, allDirsInPath, newEntry)
    //(The directory structure is immutable )
    // 4. Find  new working directory  INSTANCE given wd's  full path, in the NEW directory structures
    val newWd = newRoot.findDescendant(allDirsInPath)

    State(newRoot, newWd)
  }
  def createSpecificEntry(state: State): DirEntry
}
