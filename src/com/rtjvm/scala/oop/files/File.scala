package com.rtjvm.scala.oop.files

import com.rtjvm.scala.oop.filesystem.FileSystemException


class File(override val parentPath: String, override val name: String, val content: String) extends DirEntry(parentPath, name) {
  override def asDirectory: Directory =
    throw new FileSystemException("A file cannot be converted to a directory")

  override def asFile: File = this

  override def getType: String = "File"

  override def isDirectory: Boolean = false

  override def isFile: Boolean = true

  def setContents(newContent: String): File =
    new File(parentPath, name, newContent)

  def appendContents(newContent: String): File =
    setContents(content + "\n" + newContent)
}

object File {
  def empty(parentPath: String, name: String): File =
    new File(parentPath, name, "")
}
